const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador: null,
            numeroAtual: null,
            numeroAnterior: null,
            tamanhoDisplay:10,
            tamanhoLetra: 65 + "px",
        };

    },//Fechamento data
    methods:{
        numero(valor){
            this.displayResponsivo();

            if(this.display == "0"){
                this.display = valor.toString();
            }
            else{

                if(this.operador == "="){
                    this.display = '';
                    this.operador = null;
                }

                //this.display = this.display + valor.toString();

                //FormulaResumida
                this.display += valor.toString();
             }
        },//Fechamento numero

        decimal(){
            if(!this.display.includes(".")){
            // this.display = this.display + ".";
                //formula Resumida
                this.display += ".";
            }
        },//Fechamento decimal

        limpar(){
            this.display = "0"
            this.numeroAnterior = null;
            this.numeroAnterior = null;
            this.operador = null;
            this.displayResponsivo();
        },//Fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.numeroAtual / displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.numeroAtual * displayAtual).toString();
                        break;
                }//Fim do switch

                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;
                
                if(this.display == "NaN"){
                    this.display = "Número Indefinido!!!"
                }
                else if (this.display == "Infinity"){
                    this.display = "Sem Cabimento Meu Amigo!!";
                }
            }//Fim do if

            if(operacao != "="){
                this.operador = operacao;
                this.numeroAtual = parseFloat(this.display);
                this.display = "0";
            }
            else{
                // if(this.display.length >= this.tamanhoDisplay){
                //     alert("Limite Máximo Display ");
                // }
            

                if(this.display.includes(".")){
                    const displayAtual = parseFloat(this.display);
                    this.display = (displayAtual.toFixed(2)).toString();        
                }
                this.operador = operacao
            }
            this.displayResponsivo();
        },//fechamento operaçoes

        displayResponsivo(){
            if(this.display.length >= 31){
                this.tamanhoLetra = 14 + "px" ;
            }
            else if(this.display.length >= 20){
                this.tamanhoLetra = 20 + "px";
            }
            else if(this.display.length >= 10){
                this.tamanhoLetra = 30 + "px";
            }
            else{
                this.tamanhoLetra = 75 + "px";
            }
        },//fechamento displayResponsivo
  }//Fechamento methodes
}).mount("#app");//fechamento createApp
