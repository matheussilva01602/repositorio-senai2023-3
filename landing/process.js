const {createApp} = Vue;

createApp({
    data(){
        return{
            products:[
                {
                    id:1,
                    name:"Tenis Lamelo Ball Colorido",
                    description:"Tenis do lamelo ball original ",
                    price: 1200.00,
                    image:"./Imagens/imgTenis.jpg",
                },//Fechamento Item1
                {
                    id: 2,
                    name:"Botas de Cowboy",
                    description:"Botas de couros para agro boy",
                    price:500.00,
                    image:"./Imagens/imgBotas.jpg",
                },//Fechamento Item2
                {
                    id: 3,
                    name:"Sapatos Elegante",
                    description:"Sapatos elegantes para reuniões",
                    price:300.00,
                    image:"./Imagens/imgSapatos.jpg",
                },//Fechamento do item 3
                {
                    id: 4,
                    name:"Chinelo Nike",
                    description:"Chinelos da nike Confortavel para seu pé",
                    price:30.00,
                    image:"./Imagens/imgChinelo.jpg",
                },//Fechamento do Item4
            ],//Fechamento Products
            currentProduct:{},//Produto atual
            cart:[],
          
        };//Fechamento return
    },//Fechamento Data
    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();
    },//Fechamento mounted

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};
        },//Fechamento updateProduc
        
        addToCart(product){
            this.cart.push(product);
        }
    },//Fechamento methods
}).mount("#app");//Fechameto creatApp